var point = ee.Geometry.Point(67.10631481105544,24.889194145744288);

var s2a = ee.ImageCollection('COPERNICUS/S2_SR')
                  .filterBounds(point)
                  .filterDate('2019-07-31', '2023-07-31')
                  .select('B1','B2','B3','B4','B5','B6','B7','B8','B8A','B9','B11','B12')
                  .filter(ee.Filter.lt('CLOUDY_PIXEL_PERCENTAGE', 10));


Map.centerObject(s2a,9)
var s2a_median = s2a.median() //take the median of the dataset

var visParams = {'min': 400,'max': [4000,3000,3000],   'bands':'B8,B4,B3'}; //b8 b4 and b3 have 10 m spatial resolution

var b4 = s2a_median.select('B4');
var b5 = s2a_median.select('B5');
var b6 = s2a_median.select('B6');
var b7 = s2a_median.select('B7');
var b8 = s2a_median.select('B8');
var b9 = s2a_median.select('B9');

var ndvi = s2a_median.normalizedDifference(['B8', 'B4'])
                      .rename('NDVI');
var L = 0.5;
var savi = b8.subtract(b4).divide(b8.add(b4).add(L)).multiply(1+L);

//Palette taken from copernicus sentinal 2 website
var palette = ['FFFFFF', 'CE7E45', 'DF923D', 'F1B555', 'FCD163', '99B718',
               '74A901', '66A000', '529400', '3E8601', '207401', '056201',
               '004C00', '023B01', '012E01', '011D01', '011301'];
//Please keep in mind that for this palette, you should set your minimum visible value to 0, as it s designed for this purpose.
//This is due to it being a gradient from brown to green tones, with a heavy focus on the green side. If we would set min: -1, NDVI = 0 would already be displayed in a dark green tone.
//You can recognize this by checking the palette-section of your layer information for ndvi_3.

Map.addLayer(ndvi, {min: 0, max: 1, palette: palette}, 'NDVI Karachi');
Map.addLayer(savi, {min: 0, max: 1, palette: palette}, 'SAVI Karachi');
