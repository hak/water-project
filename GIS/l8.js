//SAVI and Bareness index. Need to use bareness index to mask SAVI. Have not implemented VNIR of ASTER stuff to get slope and drainage data. 



var point = ee.Geometry.Point(67.10631481105544,24.889194145744288);

var l8 =  ee.ImageCollection("LANDSAT/LC08/C02/T1_TOA") 
                  .filterBounds(point)
                  //.filterDate('2022-07-31', '2023-07-31')
                  //.select('B1','B2','B3','B4','B5','B6','B7','B8','B8A','B9','B11','B12')
                  //.filter(ee.Filter.lt('CLOUDY_PIXEL_PERCENTAGE', 10))
                  ;

var l8_median = l8.median() //take the median of the dataset

var visParams = {'min': 400,'max': [4000,3000,3000]}; //b8 b4 and b3 have 10 m spatial resolution

var l8_b4 = l8_median.select('B4');
var l8_b5 = l8_median.select('B5');
var l8_b6 = l8_median.select('B6');
var l8_b8 = l8_median.select('B8');

var bli = l8_b4.add(l8_b6).subtract(l8_b5);

var savi = (l8_b5.subtract(l8_b4)).divide(l8_b5.add(l8_b4).add(1)).multiply(2);

var lst = l8_median.expression(
  "TB10 + C1 * (TB10 - TB11) + C2 * (TB10 - TB11)**2 + C0 + (C3 + C4 * W) * (1 - E) + (C5 + C6 * W) * DE", 
  {
    "TB10" : l8_median.select('B10'),
    "TB11" : l8_median.select('B11'),
    "C0" : -0.268,
    "C1" : 1.378,
    "C2" : 0.183,
    "C3" : 54.30,
    "C4" : -2.238,
    "C5" : -129.20,
    "C6" : 16.40
    
  
})

//Palette taken from copernicus sentinal 2 website
var palette = ['FFFFFF', 'CE7E45', 'DF923D', 'F1B555', 'FCD163', '99B718',
               '74A901', '66A000', '529400', '3E8601', '207401', '056201',
               '004C00', '023B01', '012E01', '011D01', '011301'];
//Please keep in mind that for this palette, you should set your minimum visible value to 0, as it s designed for this purpose.
//This is due to it being a gradient from brown to green tones, with a heavy focus on the green side. If we would set min: -1, NDVI = 0 would already be displayed in a dark green tone.
//You can recognize this by checking the palette-section of your layer information for ndvi_3.

Map.addLayer(savi, {min: 0, max: 1, palette: palette}, 'BLI Karachi July 2020');
Map.addLayer(bli, {min: 0, max: 1, palette: palette}, 'SAVI Karachi July 2020');
