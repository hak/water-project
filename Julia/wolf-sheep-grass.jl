
using Agents, Random
using InteractiveDynamics, GLMakie

@agent Sheep GridAgent{2} begin
    energy::Float64
    reproduction_prob::Float64
    delta_energy::Float64
end

@agent Wolf GridAgent{2} begin
    energy::Float64
    reproduction_prob::Float64
    delta_energy::Float64
end

function initialize_model(;
                          n_sheep = 10,
                          n_wolves = 10,
                          dims = (20,20),
                          regrowth_time = 30,
                          delta_energy_sheep = 4,
                          delta_energy_wolf = 20,
                          sheep_reproduce = 0.04,
                          wolf_reproduce = 0.05,
                          seed = 3,
                          )
    rng = MersenneTwister(seed)
    space = GridSpace(dims, periodic = true)
    properties = (
        fully_grown = falses(dims),
        countdown = zeros(Int, dims),
        regrowth_time = regrowth_time,
    )

    model = ABM(Union{Sheep, Wolf}, space;
                properties, rng, scheduler = Schedulers.randomly, warn = false
                )
    for _ in 1:n_sheep
        energy = rand(model.rng, 1:(delta_energy_sheep*2)) - 1
        add_agent!(Sheep, model, energy, sheep_reproduce, delta_energy_sheep)
    end

    for _ in 1:n_wolves
        energy = rand(model.rng, 1:(delta_energy_wolf*2)) - 1
        add_agent!(Wolf, model, energy, sheep_reproduce, delta_energy_sheep)
    end

    for p in positions(model)
        fully_grown = rand(model.rng, Bool)
        countdown = fully_grown ? regrowth_time : rand(model.rng, 1:regrowth_time) - 1
        model.countdown[p...] = countdown
        model.fully_grown[p...] = fully_grown
        end
    return model
end

sheepwolfgrass = initialize_model()

function sheepwolf_step!(sheep::Sheep, model)
    randomwalk!(sheep, model)
    sheep.energy -= 1
    if sheep.energy < 0
        remove_agent!(sheep, model)
        return
    end
    eat!(sheep,model)
    if rand(model.rng) <= sheep.reproduction_prob
        reproduce!(sheep, model)
    end
end

function sheepwolf_step!(wolf::Wolf, model)
    randomwalk!(wolf, model; ifempty = false)
    wolf.energy -= 1
    if wolf.energy < 0
        remove_agent!(wolf, model)
        return
    end

    dinner = first_sheep_in_position(wolf.pos, model)
    !isnothing(dinner) && eat!(wolf, dinner, model)
    if rand(model.rng) <= wolf.reproduction_prob
        reproduce!(wolf, model)
    end
end

function first_sheep_in_position(pos, model)
    ids = ids_in_position(pos, model)
    j = findfirst(id -> model[id] isa Sheep, ids)
    isnothing(j) ? nothing : model[ids[j]]::Sheep
end


function eat!(sheep::Sheep, model)
    if model.fully_grown[sheep.pos...]
        sheep.energy += sheep.delta_energy
        model.fully_grown[sheep.pos...] = false
    end
    return
end

function eat!(wolf::Wolf, model)
    if model.fully_grown[wolf.pos...]
        wolf.energy += wolf.delta_energy
        model.fully_grown[wolf.pos...] = false
    end
    return
end

function reproduce!(agent::A, model) where {A}
    agent.energy /= 2
    id = nextid(model)
    offspring = A(id, agent.pos, agent.energy, agent.reproduction_prob, agent.delta_energy)
    add_agent_pos!(offspring, model)
    return
end


function grass_step!(model)
    @inbounds for p in positions(model)
        if !(model.fully_grown[p...])
            if model.countdown[p...] <= 0
                model.fully_grown[p...] = true
                model.countdown[p...] = model.regrowth_time
            else
                model.countdown[p...] -= 1
            end
        end
    end
end

offset(a) = a isa Sheep ? (-0.1, -0.1 * rand()) : (+0.1, +0.1*rand())
ashape(a) = a isa Sheep ? :circle : :utriangle
acolor(a) = a isa Sheep ? RGBAf(1.0, 1.0, 1.0, 0.8) : RGBAf(0.2,0.2,0.2,0.8)

grasscolor(model) = model.countdown ./ model.regrowth_time

heatkwargs = (colormap = [:brown, :green], colorrange = (0,1))

plotkwargs = (;
              ac = acolor,
              as = 25,
              am = ashape,
              offset,
              scatterkwargs = (strokewidth = 1.0, strokecolor = :black),
              heatarray = grasscolor,
              heatkwargs = heatkwargs,
              )

sheepwolfgrass = initialize_model()

fig, ax, abmobs = abmplot(sheepwolfgrass;
                          agent_step! = sheepwolf_step!,
                          model_step! = grass_step!,
                          plotkwargs...)

display(fig)
