module UA
export initialize
export agent_step!
export model_step!
export dummystep
export identify_producer

using Agents, GLMakie
using Distributions, Random
using Random: MersenneTwister
using SimpleWeightedGraphs: SimpleWeightedDiGraph
using SparseArrays: findnz


struct Method
     yield_per_m2::Float64
     water_per_plant::Float64
     power_per_plant::Float64
     plant_per_m2::Float64
     carbon_footprint_per_plant::Float64
     cost_per_plant::Float64
end

RG = Method(1.0, 1.0, 0, 1.0, 4, 0.1)

@agent Producer GridAgent{2} begin
     expenses::Float64
     method::Method
     profit::Float64
     yield::Float64

     land_utilized::Int64
     maximum_land::Int64
     coop_id::Int64
    past_profit::Float64
end

@agent Cooperative GridAgent{2} begin
     global_profit::Float64
     global_expenses::Float64
     membership_yield_contract::Int64
     num_members::Int64
     past_profit::Float64
end

function initialize(; num_producers=3,
     num_cooperatives=1,
     M=20, seed=125)
     space = GridSpace((M, M), periodic=false, metric=:chebyshev) #in pkr

     properties = Dict(
          :M => M,
          :num_producers => num_producers,
          :num_cooperatives => num_cooperatives,
          :selling_price => 40,
          :weekly_water => 0,
          :daily_water => 10,
          :graph => SimpleWeightedDiGraph(num_producers + num_cooperatives))

     rng = Random.MersenneTwister(seed)
     model = ABM(
          Union{Producer,Cooperative}, space;
          properties, rng, scheduler=Schedulers.randomly
     )

     for n in 1:num_producers
          id = n

          pos = (id, id)
          expenses = rand(3:100) #TODO + eval(:subsidy)
          land_utilized = rand(1:10) #in m^2
          maximum_land = rand(land_utilized:10) #in m^2
          profit = 0
          past_profit = 0
          yield = 0
          cooperative_id = 0

          producer = Producer(id,
               pos,
               expenses,
               RG,
               profit,
               yield,
               land_utilized,
               maximum_land,
               cooperative_id,
               past_profit)

          add_agent_single!(producer, model)
     end


     for n in 1:num_cooperatives
          id = n + num_producers
          pos = (id, id)
          global_profit = 0
          global_expenses = rand(30:100)
          membership_yield_contract = rand(10:20)
          coop_size = 3
          past_profit = 0

          coop = Cooperative(id,
               pos,
               global_profit,
               global_expenses,
               membership_yield_contract,
               0,
               past_profit)
          add_agent_single!(coop, model)
          for i in 1:coop_size
               # members[i] = random_agent(model, identify_producer)
               # add_edge!(model.graph, members[i].id, coop.id, rand(model.rng))
               # add_edge!(model.graph, coop.id, members[i].id, rand(model.rng))
               # print(members[i].id, " ", coop.id, " | ")
               member = random_agent(model, identify_producer)
               member.coop_id = id
               coop.num_members += 1

          end
          print("\n")
     end
     return model
end

function identify_producer(agent)
     return (agent isa Producer)
end

function identify_coop(agent)
     return agent isa Cooperative
end



function get_cooperatives(producer, model)
     return model[producer.coop_id]
end


function is_in_coop(agent, model)
    return (identify_producer(agent) && agent.coop_id != 0)
end

function leave_coop!(agent, model)
    coop = model[agent.coop_id]
    coop.num_members = 0
    agent.coop_id = 0
end

function join_coop!(agent, model)
    coop = random_agent(model, identify_coop)
    if (coop !== nothing)
          coop.num_members = 0
          agent.coop_id = coop.id
     end
end

distance(pos) = sqrt(pos[1]^2 + pos[2]^2)
function producer_step!(producer, model)
     if (producer.coop_id != 0)
          coop = model[producer.coop_id]
          force = (coop.pos .- producer.pos)
          new_pos = (producer.pos .+ force)
          int_pos = (convert(Int64, floor(new_pos[1])), convert(Int64, floor(new_pos[2])))
          if (0 < int_pos[1] < model.M && 0 < int_pos[2] < model.M)
               if ids_in_position(int_pos, model) == 0
                    move_agent!(producer, int_pos, model)
               end
          end
     else
          move_agent_single!(producer, model)
     end

end


function agent_step!(agent, model)
     if agent isa Producer
          producer_step!(agent, model)
     end
end


function grow_cycle!(model)
     total_daily_water = model.daily_water + model.weekly_water / 7
     for (num_agent, agent) in enumerate(allagents(model))
          if (agent isa Producer)
               max_crop = total_daily_water / agent.method.water_per_plant
               actual_crop = max_crop + rand(model.rng) #TODO check difference b/w + and *
               agent.yield = actual_crop
          end
     end
end


function model_step!(model)
     grow_cycle!(model)
     all_agents = allagents(model)
     for (num_agent, agent) in enumerate(all_agents)
          if (agent isa Producer)
              #randomize profit (reasonably) and also take taxes, don't finalize agent expenses yet
              theoretical_yield = agent.land_utilized * agent.method.plant_per_m2 #assuming productivity of 3 kg/m^2
              actual_yield = theoretical_yield + 30 * (rand(model.rng))

              if (agent.coop_id != 0)
              coop = get_cooperatives(agent, model)
              actual_yield -= coop.membership_yield_contract
              coop.past_profit = coop.global_profit
              coop_profit = coop.membership_yield_contract * model.selling_price * rand(model.rng)
              coop.global_profit += coop_profit
              end

              agent.past_profit = agent.profit
              agent.profit = actual_yield * model.selling_price * rand(model.rng)
              agent.expenses = agent.method.cost_per_plant * agent.land_utilized * agent.method.plant_per_m2
              #DON'T MINUS YET, WAIT FOR NEXT LOOP
          end
     end

    #get benefits from cooperatives
    for agent in all_agents
        if (agent isa Producer && agent.coop_id != 0)
            coop = model[agent.coop_id]
            benefits = (coop.global_profit / coop.num_members) * rand(model.rng)
            agent.expenses -= benefits
            coop.global_profit -= benefits
            coop.global_expenses += benefits
        end

        if (agent isa Producer)
        agent.profit -= agent.expenses
        end
    end
    for agent in all_agents
     if (agent isa Producer)
         if (agent.profit / agent.past_profit > 0.5 + rand(model.rng) && is_in_coop(agent, model)) #TODO Change this
             leave_coop!(agent, model)
         elseif (agent.profit / agent.past_profit < 0.5 + rand(model.rng))
             join_coop!(agent, model)
         end
     end

          for agent in all_agents
               if (agent isa Producer)
                    if (agent.coop_id != 0)
                         coop = model[agent.coop_id]
                         coop.num_members += 1
                         print(agent.id, " ", coop.id, " ", coop.num_members, "\n")
                    end
               end
          end


end
end
end
