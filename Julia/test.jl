include("rooftop-economic.jl")
import .UA
using Statistics: mean

as = 20
scatterkwargs = (strokewidth = 3.0,)
function color(a)
    UA.identify_producer(a) ? ((a.coop_id !=0) ? :red : :green) : :blue
end

function coop_size(model)
    all_agents = UA.allagents(model)
    total_size = 0
     for agent in all_agents
         if (UA.identify_producer(agent) && agent.coop_id != 0)
             total_size += 1
         end
     end
    return total_size #/ model.num_cooperatives
end

function profit(model)
    all_agents = UA.allagents(model)
    total_profit = 0
    for (num_agent, agent) in enumerate(all_agents)
        if (UA.identify_producer(agent))
            total_profit += agent.profit
        end
    end
    return total_profit / model.num_producers
end

params = Dict(
  :selling_price => 1:1:100,
  :weekly_water => 0:1:100,
  :daily_water => 0:10:100
)

# profit(a) = UA.identify_producer(a) ? a.profit : 0
# expenses(a) = UA.identify_producer(a) ? a.expenses : 0
# avg_coop_size(a) = UA.identify_coop(a) ? a.num_members : 0
# total_coop_size(a) = (UA.identify_producer(a) && a.coop_id != 0) ? 1 : 0

mdata = [(coop_size),
         (profit)]
mlabels = ["total coop_size",
           "avg profit"]



model = UA.initialize(;
                      num_producers = 100,
                      num_cooperatives = 10)

fig, abmobs = UA.abmexploration(model;
                        UA.agent_step!,
                        UA.model_step!,
                        ac=color,
                        mdata, mlabels,
                        params=params)

display(fig)
