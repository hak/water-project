#+TITLE: Towards a model of Urban Agriculture
#+AUTHOR: Hamad Asif Khan, Aqeel Mehdi, Ahmed Mujtuba
#+DATE: \today
#+bibliography: /home/hak/notes/references.bib

* Abstract
Karachi is not necessarily doomed to the fate of a concrete jungle, according to maps calcuated. Furthermore, climactic barriers to entry in water scarce regions, such as Karachi, are not necessarily significant. Additionally, software was developed for the use of policy-makers.

* Introduction
Urban Agriculture has been hypothesized to increase food sovereignity, security, and quality by many. In this paper we try to evaluate the correlation between Urban Agriculture and climate. In addition, a highly customized agent-based-model software was developed for the use of policy-makers, especially in order to determine the effectiveness of worker cooperatives. This serves a dual purpose of introducing the agent based modelling methodology to this field. This paper comes with a [[https://codeberg.org/hak/water-project][codeberg]], of which it is highly recommended to read alongside this paper. Particularly the Julia and R folders. The primary amount of effort in this project was put into producing functional software.

* Materials and methods
** Mapping
Urban Agriculture is defined as agricultural activity conducted within 10km of the city's core [cite:@the_role_of_UA_sustainable_city_discourse]. Under this definition, we wrote a program in JavaScript using Google Earth Engine (GEE). In conjunction with Sentinel-2 satellite data we calculated the Normalized Difference Vegetation Index (NDVI) and Soil Adjusted Vegitation Index (SAVI).
NDVI was calculated as
\[NDVI = \frac{NIR-RED}{NIR + RED}\]
Where NIR stands for the Near Infrared Radiation portion of the satellite reading and RED is the red portion of the satellite reading.

Generally Karachi is understood to be a "concrete jungle". As such, we had perceptions of Karachi not even being able to support any level of greenery. The following image seemed to confirm our hypothesis.

#+CAPTION: NDVI
#+ATTR_LATEX: :width 300px
[[./resources/ndvi.png]]

However, upon further research, it was found that NDVI is in fact not a good measure of areas with high bare soil [cite:@ndvi-savi]. This is because it is extremely sensitive to soil, due to its origin in forest monitoring. In order to properly generate an image, a different measure is required. This is termed SAVI.
And SAVI was calculated as
\[SAVI = \frac{NIR-RED}{NIR + RED + L}(1+L)\]
The parameter L was set to 1, in order to minimize sensitivity to bare soil [cite:@ndvi-savi].

#+CAPTION: SAVI with L = 1
#+ATTR_LATEX: :width 300px
[[./resources/savi.png]]

** Traditional Modelling
We now have to solve the problem of barriers to entry for Urban Agriculture. For this the largest to-date dataset that we are aware of was analyzed [cite:@UA-meta-analysis] in R using regression analysis, pearson correlation coefficient analysis, and p-value analysis. The dataset was then combined with WorldClim's historical 10 arc minute datasets [cite:@WorldClim]. These two datasets were combined through geocoding via nominatim's OSM, and log(yield) was compared with climactic variables. The dataset was split in order to examine the differences between rooftop (non-CEA) and non-rooftop (CEA) approaches to UA.

To begin with, there is an interesting variation between different locations. This is shown by the following graph.
#+CAPTION: The global distribution of our dataset
#+ATTR_LATEX: :width 400px
[[./resources/global.png]]

** Agent Based Modelling
Agent based models are models that, instead of modelling general behavior through the use of quantitative variables (such as with the Lotka-Volterra equations to observe predator-prey dynamics), leverage computational power to observe emergent behavior (say, the classic wolf-sheep-grass model, an implementation of which is available on our [[https://codeberg.org/hak/water-project][codeberg]]).

Agent based models have been used to attack a variety of problems such as consumer preferences [cite:@ireland-abm-ev], domestic water demand [cite:@domestic-water-demand-abm], and farmers' sustainable decisions, [cite:@farmers-sustainable-decisions-abm]

A discussion of the agent based model (ABM) follows in the next section. A specification for our (original) agent based model was first generated using the Objectives, Design, Details (ODD) protocol [cite:@grimm2012abm] and then implemented in Julia using the Agents.jl framework [cite:@Agents.jl].

The primary objective was to ascertain whether 1. people would free-load and 2. average profit would be stabilized to a significant degree by worker cooperatives.

Excluding natural causes, we seek to understand what factors might play in to geo-spatial variations. Several authors have studied the case of Cuba [cite:@viljoen2012cuba][cite:@food-sovereignty-cuba], which is noted for its state endorsed worker cooperatives. Thus this approach was investigated and compared to a completely free market, in the case of a water-scarce region.

* Primary Research Methodology
** ODD
*** Overview
**** Purpose
- To test how/whether the presence of worker cooperatives, market regulation, and water availability influences average yield, social network formation, and the environment (greenhouse gas emissions). This is to the end that we can understand the effect of economic policy that impacts inputs to UA on UA.

**** Entities, state variables, and scales
- There are producers and cooperative agents.

- There is also a method struct, for ease of use

- Producers have the variables: expenses, method, profit, yield, land-utilized, maximum-land, coop-id, past-profit.

- Cooperatives hold the variables: global-profit, global-expenses, memebrship-yield-contract, num-members, past-profit

**** Process overview and scheduling
- Each step simulates a crop cycle. The plants' water-used, power-used, carbon-footprint etc. are seeded using "sensible" values, with a subfunction/submodel responsible for creating these values based on water availibility policy (i.e. after you use a set amount of water the water is shut off).

- Each cycle "trades" occur on a random basis according to the regulated price (if any), cuts/contracts that the cooperative takes, and random market fluctuation.

- Due to their relative benefit in the cooperative, agents can choose to leave or join a new cooperative. They do this by comparing the ratio of their past profit and current profit, and whether that is greater than 0.5 + a-random-threshold.

- Cooperatives help alleviate expenses. They take a cut, trade it on their own, and then use the money to buffer the expenses of their members.

*** Design Concepts
**** Emergence
- Social network formation, environmental impacts, and average yield are all modelled as emergent properties and as such nothing is imposed.
- Individual yield is, however, imposed.
**** Adaptation
- Producers can enter and leave cooperatives based on the producers' average profit during the past cycle.
**** Objectives
- Producers maximize the utility function of profit
**** Prediction
- Indirect prediction is used. If they sense what is being done right now is not working, they change strategies. This could mean joining any of the other cooperatives or going independent or vice versa.
**** Interaction
- Within cooperatives, there is mediated interaction where they share the same resource budget.
**** Stochasticity
- The initial variables are stochastic
- Trading is done stochastically (i.e. profit has a stochastic element to it)
**** Collectives
- Cooperatives are collectives. They are explicit in this model.
**** Observation
- Average social cooperative network size
- Average profit

*** Details
**** Initialization
- The simulation state is initialized with private producers and cooperatives, which are initialized with their own number of producers.
**** Submodels
- Yield predictor submodel
- Trading submodel
** Methods
The agent based model was first implemented in Julia, and then simulated for approximately 60 time-steps. For the first half the selling price was fixed to 40 currency units (cu) by the government. Then by about time-step 30 the price was fixed to 10 cu to simulate "bad times". A complete implementation is in the Julia folder on the project [[https://codeberg.org/hak/water-project][codeberg]], split between the Julia/rooftop-economic.jl and Julia/test.jl.

Agents are programmed to behave in their own self interest, and so are expected to not achieve altruistic behavior.

* Results
** Traditional Modelling
The following correlation coefficient table was generated. As can be seen, the logarithm of the yield is best correlated to the latitude, though this is slight. The p-values shown are also quite low. This shows some degree of geo-spatial effect on yield. Notice also the (slight) negative correlation with longitude. What this means is that the relationship is not strongly linear.
#+CAPTION: The correlation coefficient table
#+ATTR_LATEX: :width 300px
[[./resources/correlation-coefficient-yield-log.png]]

Natural spline (non-linear) models were also generated, however they seemed to overfit the data.
#+CAPTION: A set of smooth natural spline models
#+ATTR_LATEX: :width 400px :center t :float nil
[[./resources/smooth-ns.png]]

The p-values for all of these are quite low.
#+CAPTION: P-values
| x     | y   |   p-value |
|-------+-----+-----------|
| yield | lon | 1.298e-12 |
| yield | lat |   2.2e-16 |

 This next graph shows that both CEA and non-CEA (rooftop and non-rooftop respectively) are similarly weakly correlated to each of the mean-temperature, mean-pressure, and mean-precipitation variables. What this means is that these differences are not explained by climactic variables.
#+CAPTION: A set of comparisons
#+ATTR_LATEX: :width 400px

In essence, we reach an interesting conclusion. It appears that our hypothesis, that climactic variables would play a strong, if not deterministic role in rooftop agricultural yields if not non-rooftop agricultural yields is false. Indeed both rooftop and non-rooftop i.e. non-controlled and controlled agriculture follow similar (slight) downward trends. Our hypothesis was that latitude and longitude are predictors for climate and socio-economic practices, both of which should influence yield. With one out of the way, we're left with the other. The new hypothesis is that cultural practices and policy are the determinors of yield, which we look at in the next section.
[[./resources/rooftop-comparison.png]]

** Agent Based Modelling
#+CAPTION: With no cooperatives. Red agents are those in a cooperative. Blue agents are cooperatives. Green agents are those who work privately.
#+ATTR_LATEX: :width 400px
[[./resources/no-coops.png]]

#+CAPTION: With cooperatives. Red agents are those in a cooperative. Blue agents are cooperatives. Green agents are those who work privately.
#+ATTR_LATEX: :width 400px
[[./resources/yes-coops.png]]

The agents were modelled to entirely behave in a self-serving manner. However, they began to develop altruistic behavior. If you compare Figure 5 to Figure 6, you can see that the people did not "free-ride" the cooperative, even in good times as compared to bad times. Indeed, the average amount of people in the cooperative stayed about the same. Furthermore, the loss in profit is buffered. However, variations in price are much higher and the maximum profit is much lower. The latter seems to be as expected.

* Discussion and limitations
** Modelling
*** Correlations of log(yield)
#+CAPTION: Histogram of yield
#+ATTR_LATEX: :width 300px
[[./resources/histogram-yield.png]]

#+CAPTION: Histogram of log(yield)
#+ATTR_LATEX: :width 300px
[[./resources/histogram-yield-log.png]]


The logarithm of the yield follows a much clearer normal distribution, which allows us to use tests like pearson's correlation coefficient, one of whose assumptions is that both variables being correlated are roughly normally distributed. Notice that one limitation of this study is that while log(yield) is normally distributed, lon and lat are not normally distributed.

*** Climactic variables
However, we quickly came to the realization that techniques such as linear and even non-linear curve fitting were not suitable for predicting yield, especially with just the latitude and longitude variables. We needed more conclusive data. For this, then, we used WorldClim's dataset and our geocoded values in order to generate new observations of temprature, percepitation, pressure, etc.

This resulted in the following chart

#+CAPTION: A set of comparisons
#+ATTR_LATEX: :width 400px
[[./resources/rooftop-comparison.png]]

** ABM
There are a few limitations that our models are subject to. Firstly, we have not subjected our model to sensitivity analysis, as in our opinion the model itself is not yet complete. Furthermore, submodels for yield and method still need to be added.
Despite this, the production of this software (of which it is recommended that the reader try), is a sign of strong progress, and it is hoped that the method will gain traction in the future for this field.
* References
#+CITE_EXPORT: csl ~/notes/ieee.csl
#+PRINT_BIBLIOGRAPHY:
